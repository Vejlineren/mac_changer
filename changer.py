import subprocess

print('Welcome to MACchanger')
print('\n')

# Use input() for Python3 function
# Use raw_input() for Python2 function

interface = input("Interface: ")
new_mac = input("New MAC: ")
print('\n')
print('Changing MAC of ' + interface + ' to ' + new_mac)
print('\n')

subprocess.call(['ifconfig',interface,'down'])
subprocess.call(['ifconfig',interface,'hw','ether',new_mac])
subprocess.call(['ifconfig',interface,'up'])

print('Done!')