import subprocess

print('Welcome to MAC Reset')
print('\n')

# Use input() for Python3 function
# Use raw_input() for Python2 function
print('Please enter the name of the interface intended to reset.')
interface = input("Interface: ")

print('\n')
print('Resetting' + interface + ' to ' + 'true hw address')
print('\n')

subprocess.call("ifconfig eth0 hw ether $(ethtool -P eth0 | awk '{print $3}')",shell=True)

print('Done!')