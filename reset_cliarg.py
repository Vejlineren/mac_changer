import subprocess
import optparse

print('Welcome to MAC Reset')
print('\n')

# Initiate parser
parser = optparse.OptionParser()

# Add argument options
parser.add_option("-i","--interface",dest="interface",help="Enter name of interface")

# Parse entered arguments
(values, arguments) = parser.parse_args()

# Save values as variables 
interface = values.interface

print('\n')
print('Resetting' + interface + ' to ' + 'true hw address')
print('\n')

# Call CLI 
subprocess.call("ifconfig eth0 hw ether $(ethtool -P eth0 | awk '{print $3}')",shell=True)

print('Done!')